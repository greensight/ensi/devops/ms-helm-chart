{{- define "nginx.conf" }}
user  nginx;
worker_processes  2;
timer_resolution 100ms;
worker_rlimit_nofile 500000;
error_log  /var/log/nginx/error.log warn;
pid        /var/run/nginx.pid;

events {
  worker_connections 12500;
  use epoll;
  multi_accept on;
}

http {
  include       /etc/nginx/mime.types;
  default_type  application/octet-stream;
  log_format  main-log  '$remote_addr - $remote_user [$time_local] "$request" '
                        '$status $body_bytes_sent $request_time "$http_referer" '
                        '"$http_user_agent" "$http_x_forwarded_for"';
  access_log  /var/log/nginx/access.log  main-log;

  {{- if .Values.logs.enabled}}
  log_format json escape=json
  '{'
    '"time_local":"$time_iso8601",'
    '"remote_addr":"$remote_addr",'
    '"remote_user":"$remote_user",'
    '"request":"$request",'
    '"status": "$status",'
    '"body_bytes_sent":"$body_bytes_sent",'
    '"request_time":"$request_time",'
    '"http_referrer":"$http_referer",'
    '"http_user_agent":"$http_user_agent"'
  '}';

  access_log  /var/www/storage/logs/nginx.json  json;
  {{- end }}


  {{ .Values.web.nginx.config.http | indent 2}}

  include /etc/nginx/conf.d/*.conf;
}
{{- end }}

{{- define "nginx_server.conf" }}
{{ .Values.web.nginx.config.server }}
{{- end }}

{{- define "hook_script.sh" }}
#!/bin/sh
set -e
{{- range $step := .Values.hook.steps }}
echo "[$(date +"%Y-%m-%d %H:%M:%S")] hook.INFO: Step {{ $step.name }}"
{{ $step.command }}
{{- end }}
{{- end }}

{{- define "entrypoint.sh" }}
#!/bin/sh
set -e
echo "[$(date +"%Y-%m-%d %H:%M:%S")] entrypoint.INFO: Entrypoint begin"
{{- range $step := .Values.app.entrypoint.steps }}
echo "[$(date +"%Y-%m-%d %H:%M:%S")] hook.INFO: Step {{ $step.name }}"
{{ $step.command }}
{{- end }}
echo "[$(date +"%Y-%m-%d %H:%M:%S")] entrypoint.INFO: Entrypoint end"
exec "$@"
{{- end }}

{{- define "vector.toml" }}
[api]
enabled = false

{{- if .Values.logs.enabled }}
[sources.app_input]
type = "file"
include = [ "/logs/laravel.log", "/logs/**/*.log" ]
max_line_bytes = 1024000
max_read_bytes = 10240
multiline.start_pattern = '^\[[0-9]{4}-[0-9]{2}-[0-9]{2}'
multiline.mode = "halt_before"
multiline.condition_pattern = '^\[[0-9]{4}-[0-9]{2}-[0-9]{2}'
multiline.timeout_ms = 1000

[transforms.app_parse]
type = "remap"
inputs = [ "app_input" ]
source = ''' . |= parse_grok!(.message, "\\[%{TIMESTAMP_ISO8601}\\] %{GREEDYDATA:env}\\.%{LOGLEVEL:level}: ")
              .namespace = get_env_var!("NAMESPACE")
              .service = get_env_var!("APP_NAME")
              del(.file)
              del(.source_type)
         '''

[sinks.app_output_elastic]
type = "elasticsearch"
inputs = [ "app_parse" ]
api_version = "v7"
compression = "none"
endpoints = [ "{{ .Values.logs.elastic_host }}" ]
mode = "bulk"
bulk.index = "{{ .Values.logs.app_index }}"
{{- if .Values.logs.elastic_user }}
auth.strategy = "basic"
auth.user = "{{ .Values.logs.elastic_user }}"
auth.password = "{{ .Values.logs.elastic_pass }}"
{{- end }}

[sources.nginx_input]
type = "file"
include = [ "/logs/nginx.json" ]
max_line_bytes = 1024000
max_read_bytes = 10240

[transforms.nginx_parse]
type = "remap"
inputs = [ "nginx_input" ]
source = '''. = merge(object!(parse_json!(.message)), .)
            .namespace = get_env_var!("NAMESPACE")
            .service = get_env_var!("APP_NAME")
            del(.message)
            del(.file)
            del(.source_type)
         '''

[transforms.nginx_filter]
type      = "filter"
inputs    = [ "nginx_parse" ]
condition = "!match(string!(.http_user_agent), r'^Prometheus')"

[sinks.nginx_output_elastic]
type = "elasticsearch"
inputs = [ "nginx_filter" ]
api_version = "v7"
compression = "none"
endpoints = [ "{{ .Values.logs.elastic_host }}" ]
mode = "bulk"
bulk.index = "{{ .Values.logs.nginx_index }}"
{{- if .Values.logs.elastic_user  }}
auth.strategy = "basic"
auth.user = "{{ .Values.logs.elastic_user }}"
auth.password = "{{ .Values.logs.elastic_pass }}"
{{- end }}
{{- end }}

{{- end }}
